//
//  MenuScene.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import SpriteKit

class MenuScene: GlobalScene {
    
    @IBOutlet var background: SimpleNode?
    @IBOutlet var logo: SimpleNode?
    
    @IBOutlet var buttonStart: ButtonNode?
    @IBOutlet var buttonRateUs: ButtonNode?
    @IBOutlet var buttonGameCenter: ButtonNode?
    
    @IBOutlet var labelBestScore: SimpleLabel?
    @IBOutlet var labelBestScoreName: SimpleLabel?
    
    override func didMove(to view: SKView) {
        initComponents()
        if GlobalSettings.iAdShow.menuScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "iAdShow"), object: nil)}
        else { NotificationCenter.default.post(name: Notification.Name(rawValue: "iAdHide"), object: nil)}
        
        if GlobalSettings.chartboostShow.menuScene { Chartboost.showInterstitial(CBLocationDefault)}
        
        if GlobalSettings.adMobShow.menuScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobShow"), object: nil)}
        else { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobHide"), object: nil)}
        
        if GlobalSettings.adMobInterstitialCall.menuScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobInterstitialCall"), object: nil)}
        
        setInteface()
    }
    
    //MARK: Set nodes
    
    func setInteface() {
        background = SimpleNode(imageName: MenuSceneSettings.imageName.background, size: MenuSceneSettings.size.background, position: MenuSceneSettings.position.background, zPosition: MenuSceneSettings.zPosition.background)
        logo = SimpleNode(imageName: MenuSceneSettings.imageName.logo, size: MenuSceneSettings.size.logo, position: MenuSceneSettings.position.logo, zPosition: MenuSceneSettings.zPosition.logo)
        
        buttonStart = ButtonNode(imageSimpleName: MenuSceneSettings.imageName.buttonStartSimple, imagePressedName: MenuSceneSettings.imageName.buttonStartPressed, size: MenuSceneSettings.size.buttonStart, position: MenuSceneSettings.position.buttonStart, zPosition: MenuSceneSettings.zPosition.buttonStart)
        buttonRateUs = ButtonNode(imageSimpleName: MenuSceneSettings.imageName.buttonRateUsSimple, imagePressedName: MenuSceneSettings.imageName.buttonRateUsPressed, size: MenuSceneSettings.size.buttonRateUs, position: MenuSceneSettings.position.buttonRateUs, zPosition: MenuSceneSettings.zPosition.buttonRateUs)
        buttonGameCenter = ButtonNode(imageSimpleName: MenuSceneSettings.imageName.buttonGameCenterSimple, imagePressedName: MenuSceneSettings.imageName.buttonGameCenterPressed, size: MenuSceneSettings.size.buttonGameCenter, position: MenuSceneSettings.position.buttonGameCenter, zPosition: MenuSceneSettings.zPosition.buttonGameCenter)
        
        labelBestScore = SimpleLabel(text: String(UserDefaults.standard.integer(forKey: "BestScore")), fontSize: MenuSceneSettings.fontSize.labelBestScore, fontColorHex: MenuSceneSettings.fontColor.labelBestScore, position: MenuSceneSettings.position.labelBestScore, zPosition: MenuSceneSettings.zPosition.labelBestScore)
        labelBestScoreName = SimpleLabel(text: MenuSceneSettings.labelText.labelBestScoreName, fontSize: MenuSceneSettings.fontSize.labelBestScoreName, fontColorHex: MenuSceneSettings.fontColor.labelBestScoreName, position: MenuSceneSettings.position.labelBestScoreName, zPosition: MenuSceneSettings.zPosition.labelBestScoreName)
        
        self.addChild(background!)
        self.addChild(logo!)
        
        self.addChild(buttonStart!)
        self.addChild(buttonRateUs!)
        self.addChild(buttonGameCenter!)
        
        self.addChild(labelBestScore!)
        self.addChild(labelBestScoreName!)
    }
    
    //MARK: Input
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        buttonStart?.changeButtonStateToSimpleState(true)
        buttonRateUs?.changeButtonStateToSimpleState(true)
        buttonGameCenter?.changeButtonStateToSimpleState(true)
        
        for touch in touches {
            let location = touch.location(in: self)
            
            if buttonStart!.contains(location) {
                sounds.playSoundByName("buttonClick")
                changeSceneToSceneName("GameScene", withAnimationName: "MoveDown")
            }
            if buttonRateUs!.contains(location) {
                sounds.playSoundByName("buttonClick")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rateUs"), object: nil)
            }
            if buttonGameCenter!.contains(location) {
                sounds.playSoundByName("buttonClick")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "showLeaderboard"), object: nil)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            if buttonStart!.contains(location) {
                buttonStart?.changeButtonStateToSimpleState(false)
                sounds.playSoundByName("buttonClickStart")
            }
            if buttonRateUs!.contains(location) {
                buttonRateUs?.changeButtonStateToSimpleState(false)
                sounds.playSoundByName("buttonClickStart")
            }
            if buttonGameCenter!.contains(location) {
                buttonGameCenter?.changeButtonStateToSimpleState(false)
                sounds.playSoundByName("buttonClickStart")
            }
        }
    }
}
