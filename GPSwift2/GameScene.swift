//
//  GameScene.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 25.12.15.
//  Copyright (c) 2015 Vladimir Vinnik. All rights reserved.
//

import SpriteKit

class GameScene: GlobalScene {
    
    //Game stats
    var score: Int = 0
    var currentThrowCount: Int = GameProcessSettings.throwCountInOneRound
    
    var gameIsPlay: Bool = true
    var tutorialShow: Bool = false
    
    var avaliableToMakeThrow: Bool = false
    
    var gameField = Array(repeating: Array(repeating: [AnyObject](), count: 7), count: 7)
    
    //Timers
    @IBOutlet var timerForChangeSceneAfterDead: Timer?
    @IBOutlet var timerForMakeThrow: Timer?
    
    //Nodes
    @IBOutlet var background: SimpleNode?
    @IBOutlet var tutorial: SimpleNode?
    @IBOutlet var gameFieldBack: SimpleNode?
    
    @IBOutlet var labelScore: SimpleLabel?
    @IBOutlet var labelThrowCount: SimpleLabel?
    
    //MARK: Init
    
    override func didMove(to view: SKView) {
        //Init componenets
        initComponents()
        if GlobalSettings.iAdShow.gameScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "iAdShow"), object: nil)}
        else { NotificationCenter.default.post(name: Notification.Name(rawValue: "iAdHide"), object: nil)}
        
        if GlobalSettings.chartboostShow.gameScene { Chartboost.showInterstitial(CBLocationDefault)}
        
        if GlobalSettings.adMobShow.gameScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobShow"), object: nil)}
        else { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobHide"), object: nil)}
        
        if GlobalSettings.adMobInterstitialCall.gameScene { NotificationCenter.default.post(name: Notification.Name(rawValue: "adMobInterstitialCall"), object: nil)}
        
        sounds.playBackgroundMusic()

        //Show tutorial
        if UserDefaults.standard.bool(forKey: "ShowTutorial") {
            tutorialShow = true;
            gameIsPlay = false;
        }
        
        //Init nodes
        setInterface()
        
        startGame()
    }
    
    //MARK: Set nodes
    
    //Interface nodes
    
    func setInterface() {
        background = SimpleNode(imageName: GameSceneSettings.imageName.background, size: GameSceneSettings.size.background, position: GameSceneSettings.position.background, zPosition: GameSceneSettings.zPosition.background)
        tutorial = SimpleNode(imageName: GameSceneSettings.imageName.tutorial, size: GameSceneSettings.size.tutorial, position: GameSceneSettings.position.tutorial, zPosition: GameSceneSettings.zPosition.tutorial)
        gameFieldBack = SimpleNode(imageName: GameSceneSettings.imageName.gameFieldBack, size: GameSceneSettings.size.gameFieldBack, position: GameSceneSettings.position.gameFieldBack, zPosition: GameSceneSettings.zPosition.gameFieldBack)
        
        labelScore = SimpleLabel(text: "0", fontSize: GameSceneSettings.fontSize.labelScore, fontColorHex: GameSceneSettings.fontColor.labelScore, position: GameSceneSettings.position.labelScore, zPosition: GameSceneSettings.zPosition.labelScore)
        labelThrowCount = SimpleLabel(text: String(currentThrowCount), fontSize: GameSceneSettings.fontSize.labelThrowCount, fontColorHex: GameSceneSettings.fontColor.labelThrowCount, position: GameSceneSettings.position.labelThrowCount, zPosition: GameSceneSettings.zPosition.labelThrowCount)
        
        self.addChild(background!)
        if tutorialShow { self.addChild(tutorial!)}
        self.addChild(gameFieldBack!)
        
        self.addChild(labelScore!)
        self.addChild(labelThrowCount!)
    }
    
    //Game nodes
    
    //MARK: Game process
    
    func startGame() {
        if (gameIsPlay) {
            fillGameField()
            avaliableToMakeThrow = true
        }
    }
    
    func endGame() {
        if (gameIsPlay) {
            //Change stats
            gameIsPlay = false
            makeScreenShot()
            sounds.playSoundByName("endGame")
            
            //Save score
            UserDefaults.standard.set(score, forKey: "CurrentScore")
            if score > UserDefaults.standard.integer(forKey: "BestScore") {
                UserDefaults.standard.set(score, forKey: "BestScore")
                UserDefaults.standard.set(true, forKey: "NewBestScore")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "submitScore"), object: nil)
            }
            else {
                UserDefaults.standard.set(false, forKey: "NewBestScore")
            }
            
            //Change scene after time
            timerForChangeSceneAfterDead = Timer.scheduledTimer(timeInterval: Double(1), target: self, selector: #selector(GameScene.changeScene), userInfo: nil, repeats: false)
        }
    }
    
    func changeScene() {
        sounds.stopbackgroundMusic()
        changeSceneToSceneName("EndScene", withAnimationName: "MoveDown")
    }
    
    func changeScore(_ value: Int) {
        if gameIsPlay {
            score += value
            labelScore?.text = String(score)
        }
    }
    
    func changeThrowCount() {
        if gameIsPlay {
            currentThrowCount -= 1
            labelThrowCount?.text = String(currentThrowCount)
        }
    }
    
    //Work with array
    
    func sortGameField() {
        for i in 0..<gameField.count {
            var findFreeSpace = false
            var currentFreeSpacePosition = 0
            
            for j in 0..<gameField.count {
                if let currentObject = getCellAt(line: j, collumn: i) {
                    if findFreeSpace {
                        currentObject.setToPosition(currentFreeSpacePosition, collumn: i, withSpeed: GameProcessSettings.timeToSortGameField)
                        gameField[currentFreeSpacePosition][i] = [currentObject]
                        gameField[j][i] = []

                        currentFreeSpacePosition += 1
                    }
                }
                else {
                    if !findFreeSpace {
                        findFreeSpace = true
                        currentFreeSpacePosition = j
                    }
                }
            }
        }
    }
    
    func fillGameField() {
        for i in 0..<gameField.count {
            for j in 0..<gameField.count {
                if getCellAt(line: i, collumn: j) == nil {
                    let currentCell = CellNode()
                    self.addChild(currentCell)
                    currentCell.setToPosition(i, collumn: j, withSpeed: GameProcessSettings.timeToFillGameField)
                    setObjectToGameField(currentCell, toLine: i, toCollumn: j)
                }
            }
        }
    }
    
    func findAndRemoveColorIn(_ line: Int, collumn: Int) {
        let currentColor = getColorCellAt(line: line, collumn: collumn)
        let arrayWithColors = NSMutableArray(array: [])
        arrayWithColors.add([line, collumn])
        
        //Find cells
        var findSomeNew = false
        repeat {
            findSomeNew = false
            for i in 0..<arrayWithColors.count {
                let currentObjectStats = arrayWithColors.object(at: i) as! NSArray
                let currentLine = currentObjectStats[0] as! Int
                let currentCollumn = currentObjectStats[1] as! Int
                
                //Find some new
                //In line
                if currentLine + 1 < gameField.count {
                    if getColorCellAt(line: currentLine + 1, collumn: currentCollumn) == currentColor {
                        if !findDublicateThisInColorLine(currentLine + 1, collumn: currentCollumn, array: arrayWithColors) {
                            arrayWithColors.add([currentLine + 1, currentCollumn])
                            findSomeNew = true
                        }
                    }
                }
                
                if currentLine - 1 >= 0 {
                    if getColorCellAt(line: currentLine - 1, collumn: currentCollumn) == currentColor {
                        if !findDublicateThisInColorLine(currentLine - 1, collumn: currentCollumn, array: arrayWithColors) {
                            arrayWithColors.add([currentLine - 1, currentCollumn])
                            findSomeNew = true
                        }
                    }
                }
                
                //In collumn
                if currentCollumn + 1 < gameField.count {
                    if getColorCellAt(line: currentLine, collumn: currentCollumn + 1) == currentColor {
                        if !findDublicateThisInColorLine(currentLine, collumn: currentCollumn + 1, array: arrayWithColors) {
                            arrayWithColors.add([currentLine, currentCollumn + 1])
                            findSomeNew = true
                        }
                    }
                }
                
                if currentCollumn - 1 >= 0 {
                    if getColorCellAt(line: currentLine, collumn: currentCollumn - 1) == currentColor {
                        if !findDublicateThisInColorLine(currentLine, collumn: currentCollumn - 1, array: arrayWithColors) {
                            arrayWithColors.add([currentLine, currentCollumn - 1])
                            findSomeNew = true
                        }
                    }
                }
            }
        } while findSomeNew == true
        
        //Remove cells
        var currentAddToScore = 1
        for i in arrayWithColors {
            let currentObjectStats = i as! NSArray
            let currentLine = currentObjectStats[0] as! Int
            let currentCollumn = currentObjectStats[1] as! Int
            let currentCell = getCellAt(line: currentLine, collumn: currentCollumn)
            
            gameField[currentLine][currentCollumn] = []
            currentCell?.run(SKAction.sequence([
                SKAction.group([
                    SKAction.scale(to: 3, duration: GameProcessSettings.timeToRemoveFindColors),
                    SKAction.fadeOut(withDuration: GameProcessSettings.timeToRemoveFindColors)]),
                SKAction.removeFromParent()]))
            
            changeScore(currentAddToScore)
            currentAddToScore += GameProcessSettings.changeOfAddingValueToTheScoreAfterGetCell
        }
    }
    
    func findDublicateThisInColorLine(_ line: Int, collumn: Int, array: NSArray) -> Bool {
        for i in array {
            let object = i as! NSArray
            if object[0] as! Int == line && object[1] as! Int == collumn { return true}
        }
        return false
    }
    
    func setObjectToGameField(_ object: CellNode, toLine line: Int, toCollumn collumn: Int) {
        gameField[line][collumn] = [object]
    }
    
    func getCellAt(line: Int, collumn:Int) -> CellNode? {
        let object: AnyObject = gameField[line][collumn] as AnyObject
        if let cell = object as? [CellNode] { if cell.count > 0 { return cell[0]}}
        return nil
    }
    
    func getColorCellAt(line: Int, collumn:Int) -> Int? {
        let cell = getCellAt(line: line, collumn: collumn)
        if cell != nil { return cell?.colorNumber}
        else { return nil}
    }
    
    func makeThrowToPosition(_ position: CGPoint) {
        if avaliableToMakeThrow {
            avaliableToMakeThrow = false
            
            var itsFind = false
            var linePosition = 0
            var collumnPosition = 0
            
            for i in 0..<gameField.count {
                for j in 0..<gameField.count {
                    if let currentObject = getCellAt(line: i, collumn: j) {
                        if currentObject.contains(position) {
                            itsFind = true
                            linePosition = i
                            collumnPosition = j
                        }
                    }
                }
            }
            
            if itsFind {
                changeThrowCount()
                makeThrowFirstStep(inLine: linePosition, andCollumn: collumnPosition)
                
                sounds.playSoundByName("cellPress")
            }
            else {
                avaliableToMakeThrow = true
            }
        }
    }
    
    func makeThrowFirstStep(inLine line: Int, andCollumn collumn: Int) {
        findAndRemoveColorIn(line, collumn: collumn)
        
        timerForMakeThrow = Timer.scheduledTimer(timeInterval: GameProcessSettings.timeToRemoveFindColors, target: self, selector: #selector(makeThrowSecondStep), userInfo: nil, repeats: false)
    }
    
    func makeThrowSecondStep() {
        sortGameField()
        
        timerForMakeThrow = Timer.scheduledTimer(timeInterval: GameProcessSettings.timeToSortGameField, target: self, selector: #selector(makeThrowThreeStep), userInfo: nil, repeats: false)
    }
    
    func makeThrowThreeStep() {
        fillGameField()
        
        timerForMakeThrow = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(makeThrowFinalStep), userInfo: nil, repeats: false)
    }
    
    func makeThrowFinalStep() {
        if currentThrowCount <= 0 {
            endGame()
        }
        else {
            avaliableToMakeThrow = true
        }
    }
    
    //MARK: Input
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if gameIsPlay {
            for touch in touches {
                let location = touch.location(in: self)
                
                makeThrowToPosition(location)
            }
        }
        else {
            if tutorialShow {
                UserDefaults.standard.set(false, forKey: "ShowTutorial");
                gameIsPlay = true;
                tutorialShow = false;
                tutorial?.removeFromParent();
                
                startGame();
            }
        }
    }
}
