//
//  EnemyNode.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import SpriteKit

class CellNode: SKSpriteNode {
    
    var colorNumber: Int = 0
    
    init() {
        super.init(texture: SKTexture.getTextureWithTrueFiltrationFromImageName(GameSceneSettings.imageName.cell1), color: UIColor.clear, size: GameSceneSettings.size.cell)
        self.position = GameSceneSettings.position.cell
        self.position = CGPoint(x: CGFloat.random(0, max: CGFloat(GlobalSettings.sizeWidth)), y: self.position.y)
        self.zPosition = GameSceneSettings.zPosition.cell
        
        getRandomColor()
    }
    
    func getRandomColor() {
        colorNumber = Int.random(1, max: 4)
        
        if colorNumber == 1 { self.texture = SKTexture.getTextureWithTrueFiltrationFromImageName(GameSceneSettings.imageName.cell1)}
        else if colorNumber == 2 { self.texture = SKTexture.getTextureWithTrueFiltrationFromImageName(GameSceneSettings.imageName.cell2)}
        else if colorNumber == 3 { self.texture = SKTexture.getTextureWithTrueFiltrationFromImageName(GameSceneSettings.imageName.cell3)}
        else { self.texture = SKTexture.getTextureWithTrueFiltrationFromImageName(GameSceneSettings.imageName.cell4)}
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("SimpleNode init(coder:) has not been implemented")}
    
    func setToPosition(_ line: Int, collumn: Int, withSpeed speed: Double) {
        let currentWidth: CGFloat
        let currentHeight: CGFloat
        
        if line == 0 { currentHeight = GameSceneSettings.position.line1}
        else if line == 1 { currentHeight = GameSceneSettings.position.line2}
        else if line == 2 { currentHeight = GameSceneSettings.position.line3}
        else if line == 3 { currentHeight = GameSceneSettings.position.line4}
        else if line == 4 { currentHeight = GameSceneSettings.position.line5}
        else if line == 5 { currentHeight = GameSceneSettings.position.line6}
        else { currentHeight = GameSceneSettings.position.line7}
        
        if collumn == 0 { currentWidth = GameSceneSettings.position.collumn1}
        else if collumn == 1 { currentWidth = GameSceneSettings.position.collumn2}
        else if collumn == 2 { currentWidth = GameSceneSettings.position.collumn3}
        else if collumn == 3 { currentWidth = GameSceneSettings.position.collumn4}
        else if collumn == 4 { currentWidth = GameSceneSettings.position.collumn5}
        else if collumn == 5 { currentWidth = GameSceneSettings.position.collumn6}
        else { currentWidth = GameSceneSettings.position.collumn7}
        
        self.run(SKAction.move(to: CGPoint(x: currentWidth, y: currentHeight), duration: speed))
    }
}
