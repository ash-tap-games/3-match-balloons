//
//  TrueCGSizeCGPoint.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import Foundation
import SpriteKit

public extension CGSize {
    public static func CGSizeWithPercent(_ width: Float, height: Float) -> CGSize {
        return CGSize(width: CGFloat(UserDefaults.standard.float(forKey: "SizeWidth") / 100 * width), height: CGFloat(UserDefaults.standard.float(forKey: "SizeHeight") / 100 * height))
    }
    
    public static func CGSizeWithPercentScaled(_ width: Float, height: Float) -> CGSize {
        return CGSize(width: CGFloat(UserDefaults.standard.float(forKey: "SizeWidth") / 100 * width), height: CGFloat(UserDefaults.standard.float(forKey: "SizeWidth") / 100 * height))
    }
}

public extension CGPoint {
    public static func CGPointWithPercent(_ width: Float, height: Float) -> CGPoint {
        return CGPoint(x: CGFloat(UserDefaults.standard.float(forKey: "SizeWidth") / 100 * width), y: CGFloat(UserDefaults.standard.float(forKey: "SizeHeight") / 100 * height))
    }
}
