//
//  GameProgressSettings.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import Foundation
import SpriteKit

struct GameProcessSettings {
    
    //Game scene
    static let timeToRemoveFindColors: Double = 0.1
    static let timeToSortGameField: Double = 0.1
    static let timeToFillGameField: Double = 0.1
    
    static let changeOfAddingValueToTheScoreAfterGetCell: Int = 1
    
    static let throwCountInOneRound: Int = 25
    
    static let cellAnimationSpeed: Double = 0.15
}