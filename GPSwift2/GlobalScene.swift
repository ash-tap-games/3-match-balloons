//
//  GlobalScene.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import SpriteKit

class GlobalScene: SKScene {
    
    //MARK: Initital components
    @IBOutlet var sounds: Sound!
    
    func initComponents() {
        sounds = Sound()
    }
    
    //MARK: Screen shot
    
    func makeScreenShot() {
        //Create the image
        UIGraphicsBeginImageContext(CGSize(width: frame.size.width, height: frame.size.height))
        self.view?.drawHierarchy(in: frame, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        //Save screen shot
        UserDefaults.standard.set(UIImagePNGRepresentation(image!), forKey: "CurrentScreenShot");
    }

    //MARK: Change scene
    
    func changeSceneToSceneName(_ name: String, withAnimationName animationName: String) {
        let scene = getSceneByName(name)
        let animation = getAnimationForChangeSceneByName(animationName)
        
        if scene != nil {
            if animation != nil { self.view?.presentScene(scene!, transition: animation!)}
            else { self.view?.presentScene(scene!)}
        }
    }
    
    func getSceneByName(_ name: String) -> SKScene? {
        if name == "GameScene" { return GameScene(size: self.size)}
        else if name == "MenuScene" { return MenuScene(size: self.size)}
        else if name == "EndScene" { return EndScene(size: self.size)}
        else { return nil}
    }
    
    func getAnimationForChangeSceneByName(_ name: String) -> SKTransition? {
        if name == "Fade" { return SKTransition.fade(withDuration: 1)}
        else if name == "MoveDown" { return SKTransition.moveIn(with: SKTransitionDirection.down, duration: 1)}
        else if name == "MoveUp" { return SKTransition.moveIn(with: SKTransitionDirection.up, duration: 1)}
        else { return nil}
    }
}
