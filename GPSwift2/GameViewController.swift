//
//  GameViewController.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 25.12.15.
//  Copyright (c) 2015 Vladimir Vinnik. All rights reserved.
//

import UIKit
import SpriteKit
import iAd
import GameKit
import GoogleMobileAds

var iADBanner: ADBannerView!

class GameViewController: UIViewController, ADBannerViewDelegate, GKGameCenterControllerDelegate, ChartboostDelegate, GADBannerViewDelegate, GADInterstitialDelegate {
    
    @IBOutlet var adMobBannerView: GADBannerView!
    @IBOutlet var adMobInterstital: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let scene = MenuScene(fileNamed:"MenuScene") {
            //Check first launch
            if !(UserDefaults.standard.bool(forKey: "HasLaunchedOnce")) {
                //First launch
                UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
                
                //Set stats
                UserDefaults.standard.set(true, forKey: "ShowTutorial");
                
                UserDefaults.standard.set(1, forKey: "CurrentSkin")
                UserDefaults.standard.set(0, forKey: "Coins")
                
                UserDefaults.standard.set(0, forKey: "CurrentScore")
                UserDefaults.standard.set(0, forKey: "BestScore")
                
                UserDefaults.standard.set(false, forKey: "Skin2IsUnlock")
                UserDefaults.standard.set(false, forKey: "Skin3IsUnlock")
                UserDefaults.standard.set(false, forKey: "Skin4IsUnlock")
            }
            
            //Configure the view
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            skView.ignoresSiblingOrder = true
            scene.scaleMode = .aspectFill
            scene.size = skView.frame.size
            
            //Get stats
            UserDefaults.standard.set(Float(skView.frame.size.width), forKey: "SizeWidth")
            UserDefaults.standard.set(Float(skView.frame.size.height), forKey: "SizeHeight")
            
            //Load other feature
            loadAds()
//            authenticateLocalPlayer()
            
            //Get links
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.iAdShow), name: NSNotification.Name(rawValue: "iAdShow"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.iAdHide), name: NSNotification.Name(rawValue: "iAdHide"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.showChartboostVideo), name: NSNotification.Name(rawValue: "showChartboostVideo"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.adMobShow), name: NSNotification.Name(rawValue: "adMobShow"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.adMobHide), name: NSNotification.Name(rawValue: "adMobHide"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.adMobInterstitialCall), name: NSNotification.Name(rawValue: "adMobInterstitialCall"), object: nil)
            
//            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.rateUs), name: NSNotification.Name(rawValue: "rateUs"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.showLeaderboard), name: NSNotification.Name(rawValue: "showLeaderboard"), object: nil)
//            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.submitScore), name: NSNotification.Name(rawValue: "submitScore"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(GameViewController.share), name: NSNotification.Name(rawValue: "share"), object: nil)

            skView.presentScene(scene)
        }
    }
    
    //MARK: ADs
    
    func loadAds() {
        //iAD
        iADBanner = ADBannerView(frame: CGRect.zero)
        iADBanner.delegate = self
        iADBanner.frame = iADBanner.frame.offsetBy(dx: 0, dy: 0.0);
        var adFrame = iADBanner.frame
        adFrame.origin.y = self.view.frame.size.height - iADBanner.frame.size.height
        iADBanner.frame = adFrame
        view.addSubview(iADBanner)
        
        //Chartboost
        //See chartboost initialisation in GPSwift2->Default->AppDelegate.swift
        
        //Google AdMob
        adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
        adMobBannerView?.adUnitID = GlobalSettings.adMobBannerUnitID
        adMobBannerView?.delegate = self
        adMobBannerView?.rootViewController = self
        adMobBannerView?.frame.origin = CGPoint(x: 0, y: CGFloat(UserDefaults.standard.float(forKey: "SizeHeight")) - adMobBannerView.frame.size.height)
        
        //Google AdMob Interstitial
        adMobInterstital = GADInterstitial.init(adUnitID: GlobalSettings.adMobInterstitialID)
        adMobInterstital?.delegate = self
        
        let request = GADRequest()
        request.testDevices = [GlobalSettings.adMobTestDeviceID, kDFPSimulatorID]
        
        view.addSubview(adMobBannerView!)
        adMobBannerView?.load(request)
        adMobInterstital?.load(request)
    }
    
    func iAdShow() { iADBanner.alpha = 1}
    func iAdHide() { iADBanner.alpha = 0}
    
    func showChartboostVideo() { Chartboost.showInterstitial(CBLocationDefault)}
    
    func adMobShow() { adMobBannerView.alpha = 1}
    func adMobHide() { adMobBannerView.alpha = 0}
    
    func adMobInterstitialCall() {adMobInterstital?.present(fromRootViewController: self)}
    
    //MARK: RateUS
    
    func rateUs() {
        let alert = UIAlertController(title: "Like this game?", message: "Rate us in app store", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            //Go to link
            UIApplication.shared.openURL(URL(string: GlobalSettings.linkToRateUs)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: GameCenter
    
    func authenticateLocalPlayer() {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(ViewController, error) -> Void in
            //1 Show login if player is not logged in
            if ViewController != nil {
                self.present(ViewController!, animated: true, completion: nil)
            }
            //2 Player is already euthenticated & logged in, load game center
            else if localPlayer.isAuthenticated {
                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer: String?, error: NSError?) -> Void in if error != nil { print(error) }} as! (String?, Error?) -> Void)
            }
            //3 Game center is not enabled on the users device
            else { print("Local player could not be authenticated, disabling game center")}
            
            if error != nil { print(error)}
        }
    }
    
    func submitScore() {
        let sScore = GKScore(leaderboardIdentifier: GlobalSettings.leaderboardId)
        sScore.value = Int64(UserDefaults.standard.integer(forKey: "BestScore"))
        
        GKScore.report([sScore], withCompletionHandler: { (error: NSError?) -> Void in
            if error != nil { print(error!.localizedDescription)}
            else { print("Score submitted")}
        } as! (Error?) -> Void)
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    func showLeaderboard() {
        let gcVC: GKGameCenterViewController = GKGameCenterViewController()
        gcVC.gameCenterDelegate = self
        gcVC.viewState = GKGameCenterViewControllerState.leaderboards
        gcVC.leaderboardIdentifier = GlobalSettings.leaderboardId
        self.present(gcVC, animated: true, completion: nil)
    }
    
    //MARK: Share
    
    func share() {
        let text: String  = GlobalSettings.shareText
        let dataImage: Data = UserDefaults.standard.object(forKey: "CurrentScreenShot") as! Data
        let image: UIImage = UIImage(data: dataImage)!
        let shareItems: Array = [image, text] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view;
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: Other

    override var shouldAutorotate : Bool { return true}

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone { return .allButUpsideDown}
        else { return .all}
    }

    override var prefersStatusBarHidden : Bool { return true}
}
