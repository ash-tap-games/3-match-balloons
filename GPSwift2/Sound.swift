//
//  Sound.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class Sound: NSObject {
    
    var getPoint = URL(fileURLWithPath: Bundle.main.path(forResource: "getPoint", ofType: "mp3")!)
    var buttonClick = URL(fileURLWithPath: Bundle.main.path(forResource: "buttonClick", ofType: "mp3")!)
    var buttonClickStart = URL(fileURLWithPath: Bundle.main.path(forResource: "buttonClickStart", ofType: "mp3")!)
    var endGame = URL(fileURLWithPath: Bundle.main.path(forResource: "endGame", ofType: "mp3")!)
    var cellPress = URL(fileURLWithPath: Bundle.main.path(forResource: "cellPress", ofType: "mp3")!)
    var backgroundMusic = URL(fileURLWithPath: Bundle.main.path(forResource: "backgroundMusic", ofType: "mp3")!)
    
    var audioPlayerGetPoint = AVAudioPlayer()
    var audioPlayerButtonClick = AVAudioPlayer()
    var audioPlayerButtonClickStart = AVAudioPlayer()
    var audioPlayerEndGame = AVAudioPlayer()
    var audioPlayerCellPress = AVAudioPlayer()
    var audioPlayerBackgroundMusic = AVAudioPlayer()
    
    override init() {
        do {
            audioPlayerGetPoint = try AVAudioPlayer(contentsOf: getPoint, fileTypeHint:nil)
            audioPlayerButtonClick = try AVAudioPlayer(contentsOf: buttonClick, fileTypeHint:nil)
            audioPlayerButtonClickStart = try AVAudioPlayer(contentsOf: buttonClickStart, fileTypeHint:nil)
            audioPlayerEndGame = try AVAudioPlayer(contentsOf: endGame, fileTypeHint:nil)
            audioPlayerCellPress = try AVAudioPlayer(contentsOf: cellPress, fileTypeHint:nil)
            audioPlayerBackgroundMusic = try AVAudioPlayer(contentsOf: backgroundMusic, fileTypeHint:nil)
            
            audioPlayerGetPoint.prepareToPlay()
            audioPlayerButtonClick.prepareToPlay()
            audioPlayerButtonClickStart.prepareToPlay()
            audioPlayerEndGame.prepareToPlay()
            audioPlayerCellPress.prepareToPlay()
            audioPlayerBackgroundMusic.prepareToPlay()
            audioPlayerBackgroundMusic.numberOfLoops = -1 //Loop bg music
        } catch {
            print("AVAudioPlayer not initialise")
        }
    }
    
    func playSoundByName(_ name: String) {
        if name == "getPoint" { audioPlayerGetPoint.play()}
        else if name == "buttonClick" { audioPlayerButtonClick.play()}
        else if name == "buttonClickStart" { audioPlayerButtonClickStart.play()}
        else if name == "endGame" { audioPlayerEndGame.play()}
        else if name == "cellPress" {
            audioPlayerCellPress.currentTime = 0.0
            audioPlayerCellPress.play()
        }
        else { print("Not found sound name")}
    }
    
    func playBackgroundMusic() { audioPlayerBackgroundMusic.play()}
    func stopbackgroundMusic() { audioPlayerBackgroundMusic.stop()}
}
