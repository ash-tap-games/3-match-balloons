//
//  InterfaceSettings.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import Foundation
import SpriteKit

struct GameSceneSettings {
    struct imageName {
        static let background = "background_GameScene"
        static let tutorial = "tutorial_GameScene"
        static let gameFieldBack = "gameFieldBack"
        
        static let cell1 = "cell1"
        static let cell2 = "cell2"
        static let cell3 = "cell3"
        static let cell4 = "cell4"
    }
    
    struct size {
        static let background = CGSize.CGSizeWithPercent(100, height: 100)
        static let tutorial = CGSize.CGSizeWithPercent(100, height: 100)
        static let gameFieldBack = CGSize.CGSizeWithPercentScaled(82, height: 82)
        
        static let cell = CGSize.CGSizeWithPercentScaled(8.5, height: 8.5)
    }
    
    struct fontSize {
        static let labelScore: CGFloat = 65
        static let labelThrowCount: CGFloat = 40
    }
    
    struct fontColor {
        static let labelScore = "#6e6d71"
        static let labelThrowCount = "#46444a"
    }
    
    struct position {
        static let background = CGPoint.CGPointWithPercent(50, height: 50)
        static let tutorial = CGPoint.CGPointWithPercent(50, height: 50)
        static let gameFieldBack = CGPoint.CGPointWithPercent(50, height: 50)
        
        static let labelScore = CGPoint.CGPointWithPercent(50, height: 90)
        static let labelThrowCount = CGPoint.CGPointWithPercent(50, height: 18)
        
        static let cell = CGPoint.CGPointWithPercent(50, height: 120)
        
        static let line1: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 31.7)
        static let line2: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 37.8)
        static let line3: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 43.9)
        static let line4: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 50)
        static let line5: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 56.1)
        static let line6: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 62.2)
        static let line7: CGFloat = CGFloat(GlobalSettings.sizeHeight / 100 * 68.3)
        
        static let collumn1: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 17.3)
        static let collumn2: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 28.2)
        static let collumn3: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 39.1)
        static let collumn4: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 50)
        static let collumn5: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 60.9)
        static let collumn6: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 71.8)
        static let collumn7: CGFloat = CGFloat(GlobalSettings.sizeWidth / 100 * 82.7)
    }
    
    struct zPosition {
        static let background: CGFloat = 0
        static let tutorial: CGFloat = 1000
        static let gameFieldBack: CGFloat = 1
        
        static let labelScore: CGFloat = 1
        static let labelThrowCount: CGFloat = 1
        
        static let cell: CGFloat = 6
    }
}

struct MenuSceneSettings {
    struct imageName {
        static let background = "background_MenuScene"
        static let logo = "logo_MenuScene"
        
        static let buttonStartSimple = "buttonStart_simple_MenuScene"
        static let buttonStartPressed = "buttonStart_pressed_MenuScene"
        static let buttonRateUsSimple = "buttonRateUs_simple_MenuScene"
        static let buttonRateUsPressed = "buttonRateUs_pressed_MenuScene"
        static let buttonGameCenterSimple = "buttonGameCenter_simple_MenuScene"
        static let buttonGameCenterPressed = "buttonGameCenter_pressed_MenuScene"
    }
    
    struct size {
        static let background = CGSize.CGSizeWithPercent(100, height: 100)
        static let logo = CGSize.CGSizeWithPercentScaled(75.6, height: 16.1)
        
        static let buttonStart = CGSize.CGSizeWithPercentScaled(27, height: 27)
        static let buttonRateUs = CGSize.CGSizeWithPercentScaled(15, height: 15)
        static let buttonGameCenter = CGSize.CGSizeWithPercentScaled(15, height: 15)
    }
    
    struct fontSize {
        static let labelBestScore: CGFloat = 45
        static let labelBestScoreName: CGFloat = 25
    }
    
    struct labelText {
        static let labelBestScoreName = "BEST"
    }
    
    struct fontColor {
        static let labelBestScore = "#46444a"
        static let labelBestScoreName = "#46444a"
    }
    
    struct position {
        static let background = CGPoint.CGPointWithPercent(50, height: 50)
        static let logo = CGPoint.CGPointWithPercent(50, height: 75)
        
        static let buttonStart = CGPoint.CGPointWithPercent(50, height: 47.5)
        static let buttonRateUs = CGPoint.CGPointWithPercent(38, height: 30)
        static let buttonGameCenter = CGPoint.CGPointWithPercent(62, height: 30)
        
        static let labelBestScore = CGPoint.CGPointWithPercent(50, height: 15)
        static let labelBestScoreName = CGPoint.CGPointWithPercent(50, height: 8.5)
    }
    
    struct zPosition {
        static let background: CGFloat = 0
        static let logo: CGFloat = 2
        
        static let buttonStart: CGFloat = 1
        static let buttonRateUs: CGFloat = 1
        static let buttonGameCenter: CGFloat = 1
        
        static let labelBestScore: CGFloat = 2
        static let labelBestScoreName: CGFloat = 2
    }
}

struct EndSceneSettings {
    struct imageName {
        static let background = "background_EndScene"
        static let logo = "logo_EndScene"
        static let bestScoreIndicator = "bestScoreIndicator_EndScene"
        
        static let buttonMenuSimple = "buttonMenu_simple_EndScene"
        static let buttonMenuPressed = "buttonMenu_pressed_EndScene"
        static let buttonRestartSimple = "buttonRestart_simple_EndScene"
        static let buttonRestartPressed = "buttonRestart_pressed_EndScene"
        static let buttonShareSimple = "buttonShare_simple_EndScene"
        static let buttonSharePressed = "buttonShare_pressed_EndScene"
        static let buttonGameCenterSimple = "buttonGameCenter_simple_EndScene"
        static let buttonGameCenterPressed = "buttonGameCenter_pressed_EndScene"
    }
    
    struct size {
        static let background = CGSize.CGSizeWithPercent(100, height: 100)
        static let logo = CGSize.CGSizeWithPercentScaled(75.6, height: 16.1)
        static let bestScoreIndicator = CGSize.CGSizeWithPercentScaled(15, height: 15)
        
        static let buttonMenu = CGSize.CGSizeWithPercentScaled(15, height: 15)
        static let buttonRestart = CGSize.CGSizeWithPercentScaled(15, height: 15)
        static let buttonShare = CGSize.CGSizeWithPercentScaled(15, height: 15)
        static let buttonGameCenter = CGSize.CGSizeWithPercentScaled(15, height: 15)
    }
    
    struct fontSize {
        static let labelCurrentScore: CGFloat = 45
        static let labelCurrentScoreName: CGFloat = 25
        static let labelBestScore: CGFloat = 45
        static let labelBestScoreName: CGFloat = 25
    }
    
    struct labelText {
        static let labelCurrentScoreName = "NOW"
        static let labelBestScoreName = "BEST"
    }
    
    struct fontColor {
        static let labelCurrentScore = "#46444a"
        static let labelCurrentScoreName = "#46444a"
        static let labelBestScore = "#46444a"
        static let labelBestScoreName = "#46444a"
    }
    
    struct position {
        static let background = CGPoint.CGPointWithPercent(50, height: 50)
        static let logo = CGPoint.CGPointWithPercent(50, height: 75)
        static let bestScoreIndicator = CGPoint.CGPointWithPercent(75, height: 46.5)
        
        static let buttonMenu = CGPoint.CGPointWithPercent(20, height: 25)
        static let buttonRestart = CGPoint.CGPointWithPercent(40, height: 25)
        static let buttonShare = CGPoint.CGPointWithPercent(60, height: 25)
        static let buttonGameCenter = CGPoint.CGPointWithPercent(80, height: 25)
        
        static let labelCurrentScore = CGPoint.CGPointWithPercent(30, height: 45.5)
        static let labelCurrentScoreName = CGPoint.CGPointWithPercent(30, height: 39)
        static let labelBestScore = CGPoint.CGPointWithPercent(70, height: 45.5)
        static let labelBestScoreName = CGPoint.CGPointWithPercent(70, height: 39)
    }
    
    struct zPosition {
        static let background: CGFloat = 0
        static let logo: CGFloat = 2
        static let bestScoreIndicator: CGFloat = 4
        
        static let buttonMenu: CGFloat = 3
        static let buttonRestart: CGFloat = 3
        static let buttonShare: CGFloat = 3
        static let buttonGameCenter: CGFloat = 3
        
        static let labelCurrentScore: CGFloat = 2
        static let labelCurrentScoreName: CGFloat = 2
        static let labelBestScore: CGFloat = 2
        static let labelBestScoreName: CGFloat = 2
    }
}