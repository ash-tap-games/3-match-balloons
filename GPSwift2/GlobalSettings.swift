//
//  GlobalSettings.swift
//  GPSwift2
//
//  Created by Vladimir Vinnik on 02.01.16.
//  Copyright © 2016 Vladimir Vinnik. All rights reserved.
//

import Foundation
import GoogleMobileAds

struct GlobalSettings {
    static let shareText = "See my score in this game!"
    static let linkToRateUs = "LINK TO RATE US"
    static let gameCenterOn = false
    static let leaderboardId = "LEADERBOARD ID"
    static let chartboostAppId = "5ab38e0ef7c1590bbef010d3"
    static let chartboostAppSignature = "d6fe3c6fe18acc9edc127165bfe338e89eae2858"
    static let adMobBannerUnitID = "ca-app-pub-1605861861953364/8288654726"
    static let adMobInterstitialID = "ca-app-pub-1605861861953364/7430513407"
    static let adMobTestDeviceID = "850fa108ad3ca507d42404ca68926476"
    
    static let nonFilteredImagesInGame = false
    
    static let fontNameInGame = "HelveticaNeue-Thin"
    
    static let sizeWidth: Float = UserDefaults.standard.float(forKey: "SizeWidth")
    static let sizeHeight: Float = UserDefaults.standard.float(forKey: "SizeHeight")
    
    struct iAdShow {
        static let gameScene = false;
        static let storeScene = false;
        static let menuScene = false;
        static let endScene = true;
    }
    
    struct chartboostShow {
        static let gameScene = false;
        static let storeScene = true;
        static let menuScene = false;
        static let endScene = true;
    }
    
    struct adMobShow {
        static let gameScene = false;
        static let storeScene = true;
        static let menuScene = false;
        static let endScene = false;
    }
    
    struct adMobInterstitialCall {
        static let gameScene = false;
        static let storeScene = false;
        static let menuScene = false;
        static let endScene = true;
    }
}
